const { app, BrowserWindow } = require('electron')

let createWindow = () => {
  let win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true
    }
  })
  win.loadURL('https://google.com')
}

app.on('ready', createWindow)