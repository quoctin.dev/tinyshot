# tinyshot

A tiny screenshot app to learn **Electron**

```
git clone https://gitlab.com/quoctin.dev/tinyshot.git
cd tinyshot
# Install dependencies
npm i
# Run the app
npm start
```